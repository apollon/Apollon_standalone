# multistage docker build

# first stage which builds the application
FROM node:16
WORKDIR /app
COPY . .
RUN yarn install --non-interactive
RUN yarn build

# second stage which creates the container image to run the application
FROM node:16
ENV WEB_APP_PATH /app/build/webapp
ENV NODE_ENV production
EXPOSE 8080
EXPOSE 8443
WORKDIR /app
COPY --from=0 /app/build /app/build
CMD [ "node", "./build/server/src/main/server.js" ]
