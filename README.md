# Apollon Standalone

Apollon Standalone is the Standalone version of the [Apollon Editor](https://gitlab.cs.ksu.edu/apollon/Apollon).

This is a fork of [ls1intum/Apollon_standalone](https://github.com/ls1intum/Apollon_standalone) that has been customized for use at K-State. The server is powered by Express with PostgreSQL for the database and Redis for storing user sessions. The major differences are:

- This uses our forked version of Apollon with support for entity relationship diagrams
- This is only accessible to users who sign in with their K-State eID
- Diagrams are stored in a database instead of the file system

# Installation

(This is for setting up Apollon in production. Scroll down for development setup.)

## With Docker

This app has a Docker image and a docker-compose.yml that demonstrates how to host this app.

To build and start the containers, simply run `docker compose build` then `docker compose up`. By default it hosts the app on http://localhost:8080. You can edit docker-compose.yml to use port 80 or use https instead.

If you only want the docker image for Apollon, run `docker build -t apollon_standalone .`. Keep in mind the app will not work without connections to Postgres and Redis.

See the required environment variables below.

## Without docker

If you don't want to use docker for whatever reason, you can build and run this manually with yarn and node. Keep in mind the app will not work without connections to Postgres and Redis.

```sh
# git clone https://gitlab.cs.ksu.edu/apollon/Apollon_standalone.git

# install production and dev dependencies
yarn install

# build the project
yarn build

# install only production dependencies for the server build
# (packages are cached and shouldn't need to download again)
cd build/server
yarn install --production

# start server
node src/main/server.js
```

## Environment Variables

- `NODE_ENV` - "production" or "development"
- `DEPLOYMENT_URL` - The base URL for accessing the app. Default: "http://loclhost:8080"
- `HTTPS_KEY` - SSL public key, only required if using HTTPS
- `HTTPS_CERT` - SSL certificate, only required if using HTTPS
- `POSTGRES_PASSWORD` - PostgreSQL password
- `POSTGRES_USER` - PostgreSQL username
- `POSTGRES_DB` - PostgreSQL database name
- `POSTGRES_HOST` - PostgreSQL hostname, URL, or IP address
- `POSTGRES_PORT` - PostgreSQL port
- `REDIS_PASSWORD` - Redis password
- `REDIS_HOST` - Redis hostname, URL, or IP address
- `REDIS_PORT` - Redis port

# Development

## Structure

This project consists of three packages:

- `server` - server-side Express app
- `webapp` - client-side React app
- `shared` - code shared between the two

## Developer Setup

We strongly recommend using the included [VS Code dev container](https://code.visualstudio.com/docs/devcontainers/containers) configuration. It automatically runs the necessary Postgres and Redis containers, configures VS Code extensions and settings, and is almost identical to the environment this app will use in production.

See the page in the Apollon wiki for more info. The basic steps are:

1. Install the Dev Containers extension in VS Code
2. Clone this repo and open it in Code
3. Click the remote button in the bottom-left, then click Reopen in Container

This project uses [Yarn](https://classic.yarnpkg.com/lang/en/) (v1, not v2+) as its package manager. **Do not use npm**; it will cause issues.

Run these yarn commands as needed:

```sh
# install dependencies
yarn install # or just `yarn`

# build the project
yarn build

# start the server and client apps on http://localhost:8080
yarn start

# see the package.json files for more specific commands.
```

### Debugging

There are three debug profiles in VS Code:

- Debug Server - Runs `yarn start` for debugging server-side code
- Launch Chrome and Launch Firefox - Launches and connects to the browser to debug client-side code. Debug Server or `yarn start` should be run first.

### CAS Auth Server

For dev/testing purposes, starting the dev server will also start a mock CAS server (on http://localhost:3004) instead of using K-State's CAS server. Right now the test users are `user1`, `user2`, and `user3`. You can also edit `packages/server/cas-mock-users.json` if you'd like to add other users.

To switch to K-State's CAS server, set `NODE_ENV` to `production` (e.g. `NODE_ENV=production yarn start`).

### TypeORM

This app uses [TypeORM](https://typeorm.io/) to interface with the Postgres database. TypeORM-relevant code is in `packages/server/src/main/services/storage-service`

- `data-source.ts` - [Data Source](https://typeorm.io/data-source) that interacts with the database (handles the connection, entites, migrations, etc.)
- `entities/*.ts` - [Entities](https://typeorm.io/entities) that define the tables/relationships in the database
- `migrations/*.ts` - Generated [migrations](https://typeorm.io/migrations) for updating schemas

If any entites were added or modified, the database will need to be updated with the new schema by using a migration. Migrations can be created manually, but you probably want to use the typeorm CLI to generate one automatically:

```sh
# from packages/server/
# replace AddDiagrams with the relevant change
yarn typeorm migration:generate -d src/main/services/storage-service/data-source.ts src/main/services/storage-service/migrations/AddDiagrams
```

Every time it connects to the database, the Data Source is configured to apply all migrations in the `migrations` directory if they haven't been applied yet, so you do not need to run the migrations manually.

> Actually, by default the `typeorm` cli will not work with typescript files (??????) but only javascript files. `package.json` aliases `typeorm` to `typeorm-ts-node-commonjs` (which uses ts-node instead of node so it works with typescript) so you shouldn't have to worry about this.
