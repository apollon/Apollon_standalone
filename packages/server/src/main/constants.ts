import path from 'path';

export const webappPath = process.env.WEB_APP_PATH ? process.env.WEB_APP_PATH : path.resolve(__dirname, `../../../../build/webapp`);

export const indexHtml = path.resolve(webappPath, `./index.html`);

export const diagramStoragePath = path.resolve(__dirname, `../../../../diagrams`);

export const tokenLength = 20;

export const appBaseUrl = process.env.DEPLOYMENT_URL ? process.env.DEPLOYMENT_URL : 'http://localhost:8080';
