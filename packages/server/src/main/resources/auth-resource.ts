import { Application, RequestHandler } from 'express';
import passport from 'passport';
import { AuthService } from '../services/auth-service/auth-service';
import { KsuAuthService } from '../services/auth-service/ksu-auth-service';
import { SessionService } from '../services/session-service/session-service';
import { RedisSessionService } from '../services/session-service/redis-session-service';
// import { MemorySessionService } from '../services/session-service/memory-session-service';

export type { KsuUser as User } from '../services/auth-service/ksu-auth-service';

export class AuthResource {
  authService: AuthService;
  sessionService: SessionService = new RedisSessionService();

  loginHandler: RequestHandler;
  logoutHandler: RequestHandler;
  sendInfoHandler: RequestHandler;

  requireAuth: RequestHandler = (req, res, next) => {
    if (!req.user) {
      return this.loginHandler(req, res, next);
    }
    return next();
  };

  constructor(app: Application) {
    app.use(passport.initialize());
    this.sessionService.register(app);
    app.use(passport.session());

    this.authService = new KsuAuthService();

    this.loginHandler = this.authService.loginHandler;
    this.logoutHandler = this.authService.logoutHandler;
    this.sendInfoHandler = this.authService.sendInfoHandler;
  }
}
