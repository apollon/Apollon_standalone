import express from 'express';
import cors from 'cors';
import { DiagramResource } from './resources/diagram-resource';
import { ConversionResource } from './resources/conversion-resource';
import { AuthResource } from './resources/auth-resource';

// options for cors midddleware
const options: cors.CorsOptions = {
  allowedHeaders: ['Origin', 'X-Requested-With', 'Content-Type', 'Accept', 'X-Access-Token'],
  credentials: true,
  methods: 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE',
  preflightContinue: false,
};

export const register = (app: express.Application) => {
  const diagramResource = new DiagramResource();
  const conversionResource = new ConversionResource();
  const authResource = new AuthResource(app);
  const router = express.Router();
  router.use(cors(options));

  // auth
  app.get('/login', authResource.loginHandler);
  app.post('/logout', authResource.logoutHandler);
  app.use('/*', authResource.requireAuth);
  app.use('/loginInfo', authResource.sendInfoHandler);

  // diagrams
  router.post('/converter/pdf', (req, res) => conversionResource.convert(req, res));
  router.get('/converter/status', (req, res) => conversionResource.status(req, res));
  router.get('/diagrams/byuser', (req, res) => diagramResource.getDiagramsByUser(req, res));
  router.get('/diagrams/:token', (req, res) => diagramResource.getDiagram(req, res));
  router.post('/diagrams/publish', (req, res) => diagramResource.publishDiagram(req, res));
  router.post('/diagrams/pdf', (req, res) => diagramResource.convertSvgToPdf(req, res));
  router.post('/diagrams/remote-save', (req, res) => diagramResource.saveDiagram(req, res));
  app.use('/api', router);
};
