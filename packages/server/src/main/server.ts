import bodyParser from 'body-parser';
import express, { Express, RequestHandler } from 'express';
import { indexHtml, webappPath } from './constants';
import { register } from './routes';
import { CollaborationService } from './services/collaboration-service/collaboration-service';
import * as http from "http";
import * as https from "https";

const port = 8080;
const portsecure = 8443;

const credentials = { key: process.env.HTTPS_KEY, cert: process.env.HTTPS_CERT }

export const app: Express = express();

app.use(bodyParser.json() as RequestHandler);
app.use(
  bodyParser.urlencoded({
    extended: true,
  }) as RequestHandler,
);

// register routes
register(app);
app.use('/', express.static(webappPath));

// if nothing matches return webapp
// must be registered after other routes
app.get('/*', (req, res) => res.sendFile(indexHtml));

const collaborationService = new CollaborationService();

const server = http.createServer(app).listen(port);
server.on('upgrade', (request, socket, head) => {
  collaborationService.handleUpgrade(request, socket, head);
});

const serverSecure = https.createServer(credentials, app).listen(portsecure);
serverSecure.on('upgrade', (request, socket, head) => {
  collaborationService.handleUpgrade(request, socket, head);
});
