import { RequestHandler } from 'express';

export interface AuthService {
  loginHandler: RequestHandler;
  logoutHandler: RequestHandler;
  sendInfoHandler: RequestHandler;
}
