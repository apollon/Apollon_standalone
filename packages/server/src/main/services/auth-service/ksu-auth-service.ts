import { AuthService } from './auth-service';
import { CasStrategy } from './passport-cas-strategy';
import passport from 'passport';
import { NextFunction, Request, Response } from 'express';
import { appBaseUrl } from '../../constants';

const casOptions = {
  ssoBaseUrl: process.env.NODE_ENV === 'production' ? 'https://signin.k-state.edu/WebISO' : 'http://localhost:3004',
  ssoLoginUrl:
    process.env.NODE_ENV === 'production' ? 'https://signin.k-state.edu/WebISO/login' : 'http://localhost:3004/login',
  validateEndpoint: '/serviceValidate',
  appServiceUrl: appBaseUrl + '/login/',
};

export interface KsuUser {
  eid: string;
}

export class KsuAuthService implements AuthService {
  cas: CasStrategy;

  constructor() {
    this.cas = new CasStrategy(casOptions, (userAttribs, done) => {
      if (!userAttribs) {
        done(new Error('CAS attributes undefined'));
        return;
      }

      let user: KsuUser = { eid: userAttribs.user };

      done(null, user);
    });

    passport.use(this.cas);

    passport.serializeUser((user, done) => {
      process.nextTick(() => done(null, { eid: (user as KsuUser).eid }));
    });

    passport.deserializeUser((user, done) => {
      process.nextTick(() => done(null, user as KsuUser));
    });
  }

  loginHandler(req: Request, res: Response, next: NextFunction) {
    let handler = passport.authenticate('cas2', {
      failureRedirect: '/',
      failureMessage: true, // TODO: display failure message on page
      // TODO: return to the original URL the user came from, unless they went directly to the login page
      successRedirect: req.originalUrl.startsWith('/login') ? '/' : req.originalUrl,
      //successRedirect: '/',
    });
    handler(req, res, next);
  }

  // TODO: handle user logging out outisde of Apollon
  logoutHandler(req: Request, res: Response, next: NextFunction) {
    req.logout((err: Error) => {
      if (err) {
        return next(err);
      }
      var redirectURL = casOptions.ssoBaseUrl + `/logout?url=${appBaseUrl}`;
      res.redirect(redirectURL);
    });
  }

  // Sends user information to the React frontend through endpoint "/loginInfo" (right now, just EID of the user)
  sendInfoHandler(req: Request, res: Response) {
    res.json({ eid: (req.user as KsuUser).eid });
  }
}
