import { randomString } from '../../utils';
import { tokenLength } from '../../constants';
import { DiagramStorageService } from '../diagram-storage/diagram-storage-service';
import { DiagramDTO } from 'shared/src/main/diagram-dto';
import { UserDiagramsResultDTO } from 'shared/src/main/user-diagrams-result-dto';

export class DiagramService {
  private storageService: DiagramStorageService;

  constructor(storageService: DiagramStorageService) {
    this.storageService = storageService;
  }

  saveDiagramAndGenerateTokens(diagramDTO: DiagramDTO, user?: string): Promise<string> {
    // alpha numeric token with length = tokenLength
    const token = randomString(tokenLength);
    return this.storageService.saveDiagram(diagramDTO, token, false, user);
  }

  getDiagramByLink(token: string, user?: string): Promise<DiagramDTO | undefined> {
    return this.storageService.getDiagramByLink(token, user);
  }

  getDiagramsByUser(user: string): Promise<UserDiagramsResultDTO[]> {
    return this.storageService.getDiagramsByUser(user);
  }
}
