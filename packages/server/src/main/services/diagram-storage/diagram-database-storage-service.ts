import { DiagramDTO } from '../../../../../shared/src/main/diagram-dto';
import { DatabaseStorageService } from '../storage-service/database-storage-service';
import { DiagramStorageService } from './diagram-storage-service';
import { UMLModel } from '@ls1intum/apollon';
import moment from 'moment';
import { Diagram } from '../storage-service/entities/diagram-entity';
import { UserDiagramsResultDTO } from '../../../../../shared/src/main/user-diagrams-result-dto';

export class DiagramDatabaseStorageService implements DiagramStorageService {
  private databaseStorageService = new DatabaseStorageService();

  saveDiagram(diagramDTO: DiagramDTO, token: string, _shared?: boolean, user?: string): Promise<string> {
    if (user === undefined) {
      throw new Error(`user required when using the database storage service`);
    }
    return this.databaseStorageService.saveDiagram(
      token,
      user,
      diagramDTO.id,
      diagramDTO.title,
      JSON.stringify(diagramDTO.model),
      new Date(diagramDTO.lastUpdate.toString()),
    );
  }

  getDiagramByLink(token: string, user?: string | undefined): Promise<DiagramDTO> {
    if (user === undefined) {
      throw new Error(`user required when using the database storage service`);
    }
    return this.databaseStorageService.getDiagramByToken(token, user).then((diagram: Diagram) => {
      return new DiagramDTO(
        diagram.uuid,
        diagram.title,
        JSON.parse(diagram.model) as UMLModel,
        moment(diagram.lastUpdate),
      );
    });
  }

  getDiagramsByUser(user: string): Promise<UserDiagramsResultDTO[]> {
    return this.databaseStorageService.getDiagramsByUser(user).then((diagrams: Diagram[]) => {
      return diagrams.map(
        (diagram) => new UserDiagramsResultDTO(diagram.uuid, diagram.token, diagram.title, moment(diagram.lastUpdate)),
      );
    });
  }
}
