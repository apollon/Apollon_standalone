import { DiagramDTO } from 'shared/src/main/diagram-dto';
import { UserDiagramsResultDTO } from 'shared/src/main/user-diagrams-result-dto';

export interface DiagramStorageService {
  saveDiagram(diagramDTO: DiagramDTO, token: string, shared?: boolean, user?: string): Promise<string>;
  getDiagramByLink(token: string, user?: string): Promise<DiagramDTO | undefined>;
  getDiagramsByUser(user: string): Promise<UserDiagramsResultDTO[]>;
}
