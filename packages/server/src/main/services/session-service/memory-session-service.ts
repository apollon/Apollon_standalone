import session from 'express-session';
import { SessionService } from './session-service';

export class MemorySessionService extends SessionService {
  constructor() {
    super();
    // default session options use memory storage
    this.session = session(this.defaultSessionOptions);
  }
}
