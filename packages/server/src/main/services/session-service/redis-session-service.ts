import session from 'express-session';
import connectRedis from 'connect-redis';
import { createClient } from 'redis';
import { SessionService } from './session-service';

const RedisStore = connectRedis(session);

export class RedisSessionService extends SessionService {
  constructor() {
    super();
    const redisClient = createClient({
      legacyMode: true,
      socket: {
        host: process.env.REDIS_HOST,
        port: process.env.REDIS_PORT ? parseInt(process.env.REDIS_PORT) : undefined,
      },
      password: process.env.REDIS_PASSWORD,
    });
    redisClient.connect();
    this.session = session({
      ...this.defaultSessionOptions,
      store: new RedisStore({ client: redisClient }),
    });
  }
}
