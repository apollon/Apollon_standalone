import { Application, RequestHandler } from 'express';
import session from 'express-session';
import cookieParser from 'cookie-parser';

export abstract class SessionService {
  protected session: RequestHandler;
  protected defaultSessionOptions: session.SessionOptions = {
    secret: 'super secret password that i will definitely remember to change',
    saveUninitialized: false,
    cookie: { maxAge: 1000 * 60 * 60 * 24 }, // one day
    resave: false,
  };

  register(app: Application) {
    app.use(cookieParser());
    app.use(this.session);
  }
}
