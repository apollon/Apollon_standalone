import { DataSource } from 'typeorm';
import { Diagram } from './entities/diagram-entity';
console.log(__dirname + 'migrations/*.*');

export const AppDataSource = new DataSource({
  type: 'postgres',
  host: process.env.POSTGRES_HOST,
  port: process.env.POSTGRES_PORT ? parseInt(process.env.POSTGRES_PORT) : undefined,
  username: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  database: process.env.POSTGRES_DB,
  synchronize: false, // use migrations instead
  logging: true,
  entities: [Diagram],
  subscribers: [],
  migrations: [__dirname + '/migrations/*.[tj]s'],
  migrationsRun: true,
});

AppDataSource.initialize().then(() => {
  console.log('Database connected');
});
