import { Repository } from 'typeorm';
import { Diagram } from './entities/diagram-entity';
import { AppDataSource } from './data-source';

export class DatabaseStorageService {
  private diagramRepository: Repository<Diagram>;

  constructor() {
    this.diagramRepository = AppDataSource.getRepository(Diagram);
  }

  async saveDiagram(token: string, user: string, uuid: string, title: string, model: string, lastUpdate: Date) {
    const existingDiagram = await this.diagramRepository.findOneBy({ uuid });
    if (existingDiagram !== null) {
      if (existingDiagram.user !== user) {
        throw new Error(`Ddiagram ${uuid} exists and belongs to another user`);
      }

      // Update existing diagram
      // TODO: Keep existing token when re-sharing diagrams
      //   because the previous token will no longer work.
      //   Need to make sure webapp uses the existing token and not the new one.
      existingDiagram.uuid = uuid;
      existingDiagram.title = title;
      existingDiagram.model = model;
      existingDiagram.lastUpdate = lastUpdate;
      return this.diagramRepository.save(existingDiagram).then(() => existingDiagram.token);
    }

    // Create new diagram
    const diagram = new Diagram();
    diagram.token = token;
    diagram.user = user;
    diagram.uuid = uuid;
    diagram.title = title;
    diagram.model = model;
    diagram.lastUpdate = lastUpdate;
    return this.diagramRepository
      .save(diagram)
      .then(() => token)
      .catch((err: Error) => {
        throw err;
      });
  }

  async getDiagramByToken(token: string, user: string): Promise<Diagram> {
    const diagram = await this.diagramRepository.findOneByOrFail({
      token,
      user,
    });
    return diagram;
  }

  async getDiagramsByUser(user: string): Promise<Diagram[]> {
    // TODO: pagination https://typeorm.io/select-query-builder#using-pagination
    const diagrams = await this.diagramRepository.findBy({ user });
    return diagrams;
  }
}
