import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, Unique } from 'typeorm';

@Entity({ name: 'diagrams' })
@Unique(['user', 'title'])
export class Diagram {
  @PrimaryGeneratedColumn()
  id: number;

  /** Alpha-numeric token used in the URL for the diagram */
  @Column({ unique: true })
  token: string;

  /** User that owns the diagram */
  @Column()
  user: string;

  /** The ID in the DiagramDTO object; NOT the same as this entity's primary key ID */
  @Column('uuid', { unique: true })
  uuid: string;

  /** The title of the diagram */
  @Column()
  title: string;

  /** UMLModel object as JSON */
  @Column('json')
  model: string;

  @CreateDateColumn()
  createDate: Date;

  /** The last time the model was updated.
   *
   * The existing DiagramDTO class already has a lastUpdate field, so we use that instead of an auto-generated one.
   */
  @Column('timestamp')
  lastUpdate: Date;

  // TODO: sharing options, max length constraints
}
