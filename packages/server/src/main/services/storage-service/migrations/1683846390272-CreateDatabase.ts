import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateDatabase1683846390272 implements MigrationInterface {
    name = 'CreateDatabase1683846390272'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "diagrams" ("id" SERIAL NOT NULL, "token" character varying NOT NULL, "user" character varying NOT NULL, "uuid" uuid NOT NULL, "title" character varying NOT NULL, "model" json NOT NULL, "createDate" TIMESTAMP NOT NULL DEFAULT now(), "lastUpdate" TIMESTAMP NOT NULL, CONSTRAINT "UQ_0de9f8185098a4f2a67acc0b58e" UNIQUE ("token"), CONSTRAINT "UQ_75c5f0c7fa064e14336694dd9de" UNIQUE ("uuid"), CONSTRAINT "UQ_43be1263cc4e30b9114242ccf30" UNIQUE ("user", "title"), CONSTRAINT "PK_81f832a385d660caf0bf53cb6c9" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "diagrams"`);
    }

}
