import { Moment } from 'moment';

export class UserDiagramsResultDTO {
  id: string;
  title: string;
  lastUpdate: Moment;
  token: string;

  constructor(id: string, token: string, title: string, lastUpdate: Moment) {
    this.id = id;
    this.title = title;
    this.lastUpdate = lastUpdate;
    this.token = token;
  }
}
