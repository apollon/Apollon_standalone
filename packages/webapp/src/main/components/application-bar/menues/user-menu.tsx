import React, { Component, useState } from 'react';
import Button from 'react-bootstrap/Button';
import { ModalRepository } from '../../../services/modal/modal-repository';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { ApplicationState } from '../../store/application-state';
import { ModalContentType } from '../../modals/application-modal-types';
import { NavDropdown } from 'react-bootstrap';

// Additional variable
type OwnProps = {};

// State values of the component
type State = {
  show: boolean;
  eid: string;
};

// Additional variable
type StateProps = {};

// Props handled in other components
type DispatchProps = {
  openModal: typeof ModalRepository.showModal;
};

// Joins all props together
type Props = OwnProps & StateProps & DispatchProps;

// Connects the component to a Redux store
const enhance = connect<StateProps, DispatchProps, OwnProps, ApplicationState>(null, {
  openModal: ModalRepository.showModal,
});

// Creates a user menu component class
class UserMenuComponent extends Component<Props, State> {
  // Constructs a User Menu
  constructor(props: Props) {
    super(props);
    // Initializes properties
    this.state = { show: false, eid: '' };
  }

  // Handles mounting component
  componentDidMount() {
    // Fetches information about the user from the Express server authentication (specifically, EID for right now)
    fetch('/loginInfo', {
      credentials: 'same-origin',
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    }).then((res) => res.json().then((json) => this.setState({ eid: json.eid })));
    document.addEventListener('click', this.hideMenu);
  }

  // Handles unmounting component
  componentWillUnmount() {
    document.removeEventListener('click', this.hideMenu);
  }

  // Handles showing the menu when required
  showMenu = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
    this.setState({ show: true });
    event.stopPropagation();
  };

  // Handles hiding the menu when required
  hideMenu = (event: MouseEvent) => {
    this.setState({ show: false });
    event.stopPropagation();
  };

  // Logs the user out of the K-State authentication service
  logout = () => {
    const form = document.createElement('form');
    form.method = 'POST';
    form.action = '/logout';
    document.body.appendChild(form);
    form.submit();
  };

  // Renders the actual HTML Element with our required functionality
  render() {
    return (
      <>
        <NavDropdown id="user-menu-item" title={this.state.eid} onClick={this.showMenu}>
          <NavDropdown.Item onClick={this.logout}>Logout</NavDropdown.Item>
        </NavDropdown>
      </>
    );
  }
}

// Exports the component as UserMenu
export const UserMenu = enhance(UserMenuComponent);
