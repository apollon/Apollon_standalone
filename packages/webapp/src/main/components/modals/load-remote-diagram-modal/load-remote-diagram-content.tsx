import React from 'react';
import { ListGroup } from 'react-bootstrap';
import { UserDiagramsResultDTO } from 'shared/src/main/user-diagrams-result-dto';
import { LocalStorageDiagramListItem } from '../../../services/local-storage/local-storage-types';
import { LoadRemoteDiagramItem } from './load-remote-diagram-item';

type Props = {
  diagrams: UserDiagramsResultDTO[] | null;
  onSelect: (token: string) => void;
};

export const LoadRemoteDiagramContent = (props: Props) => {
  if (props.diagrams === null) {
    return <p>Diagrams not loaded.</p>;
  } else if (props.diagrams.length === 0) {
    return <p>No diagrams to load.</p>;
  } else {
    return (
      <ListGroup>
        {props.diagrams.map((value) => (
          <ListGroup.Item key={value.id} action onClick={(_event: any) => props.onSelect(value.token)}>
            <LoadRemoteDiagramItem item={value} />
          </ListGroup.Item>
        ))}
      </ListGroup>
    );
  }
};
