import { Component, ComponentClass } from 'react';
import { LocalStorageRepository } from '../../../services/local-storage/local-storage-repository';
import { ModalContentProps } from '../application-modal-types';
import { Button, Modal } from 'react-bootstrap';
import React from 'react';
import { compose } from 'redux';
import { withApollonEditor } from '../../apollon-editor-component/with-apollon-editor';
import { RemoteStorageRepository } from '../../../services/remote-storage/remote-storage-repository';
import { connect } from 'react-redux';
import { ApplicationState } from '../../store/application-state';
import { UserDiagramsResultDTO } from 'shared/src/main/user-diagrams-result-dto';
import { LoadRemoteDiagramContent } from './load-remote-diagram-content';

type OwnProps = {} & ModalContentProps;

type State = {};

type StateProps = {
  currentDiagramId?: string;
  userDiagrams: UserDiagramsResultDTO[] | null;
};

type DispatchProps = {
  load: typeof RemoteStorageRepository.load;
  getRemote: typeof RemoteStorageRepository.getUserDiagrams;
};

type Props = StateProps & OwnProps & DispatchProps;

const getInitialState = (): State => {
  return {
    userDiagrams: null,
  };
};

const enhance = compose<ComponentClass<Props>>(
  withApollonEditor,
  connect<StateProps, DispatchProps, Props, ApplicationState>(
    (state) => ({
      currentDiagramId: state.diagram?.id,
      userDiagrams: state.remoteStorage.fetchedUserDiagrams,
    }),
    {
      load: RemoteStorageRepository.load,
      getRemote: RemoteStorageRepository.getUserDiagrams,
    },
  ),
);

class LoadRemoteDiagramModalComponent extends Component<Props, State> {
  state = getInitialState();

  handleClose = () => {
    this.props.close();
    this.setState(getInitialState());
  };

  refreshClicked = (_event: any) => {
    this.props.getRemote();
  };

  loadDiagram = (token: string) => {
    if (token) {
      this.props.load(token);
    }
    this.handleClose();
  };

  render() {
    return (
      <>
        <Modal.Header closeButton>
          <Modal.Title>Load Remote Diagram</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <LoadRemoteDiagramContent diagrams={this.props.userDiagrams} onSelect={this.loadDiagram} />
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.refreshClicked} className="btn-secondary">
            Refresh
          </Button>
        </Modal.Footer>
      </>
    );
  }
}

export const LoadRemoteDiagramModal = enhance(LoadRemoteDiagramModalComponent);
