import { LocalStorageActions } from './local-storage/local-storage-types';
import { Action } from 'redux';
import { EditorOptionsActions } from './editor-options/editor-options-types';
import { ImportActions } from './import/import-types';
import { DiagramActions } from './diagram/diagram-types';
import { ErrorActions } from './error-management/error-types';
import { ModalActions } from './modal/modal-types';
import { ShareActions } from './share/share-types';
import { RemoteStorageActions } from './remote-storage/remote-storage-types';
export type Actions =
  | LocalStorageActions
  | RemoteStorageActions
  | StopAction
  | EditorOptionsActions
  | ImportActions
  | DiagramActions
  | ErrorActions
  | ModalActions
  | ShareActions
  | RemoteStorageActions;

export const enum StopActionType {
  STOP_ACTION = '@@stop_action',
}
export type StopAction = Action<StopActionType.STOP_ACTION>;
