import {
  LoadAction,
  RemoteStorageActionTypes,
  RemoteStorageDiagramListItem,
  StoreAction,
} from './remote-storage-types';
import { ApplicationState } from '../../components/store/application-state';
import { combineEpics, Epic, ofType } from 'redux-observable';
import { Action } from 'redux';
import { filter, map, mergeMap } from 'rxjs/operators';
import { StopAction, StopActionType } from '../actions';
import moment from 'moment';
import { Diagram, UpdateDiagramAction } from '../diagram/diagram-types';
import { DiagramRepository } from '../diagram/diagram-repository';
import { ErrorActionType, DisplayErrorAction } from '../error-management/error-types';
import { ErrorRepository } from '../error-management/error-repository';
import { of } from 'rxjs';
import { EditorOptionsRepository } from '../editor-options/editor-options-repository';
import { ChangeDiagramTypeAction } from '../editor-options/editor-options-types';
import { BASE_URL } from '../../constant';
import { Observable, concatAll } from 'rxjs';
import { GetUserDiagramsAction, GetUserDiagramsDoneAction } from './remote-storage-types';
import { UserDiagramsResultDTO } from 'shared/src/main/user-diagrams-result-dto';
import { RemoteStorageRepository } from './remote-storage-repository';

// Handles what happens when the application undergoes the "Store" action
// In this case, it stores the diagram to the remote server
export const storeEpic: Epic<Action, StopAction, ApplicationState> = (
  action$: Observable<Action<RemoteStorageActionTypes>>,
  store,
) => {
  return action$.pipe(
    filter((action) => action.type === RemoteStorageActionTypes.STORE),
    map((action) => action as StoreAction),
    map((action: StoreAction) => {
      const { diagram } = action.payload;
      diagram.lastUpdate = moment();

      const resourceUrl = `${BASE_URL}/diagrams/remote-save`;

      const body = JSON.stringify(diagram);

      // Save to database here and update latest diagram entry
      // Send diagram json string to our endpoint in the diagram repository then store its response
      const _response = fetch(resourceUrl, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body,
      }).then((response: Response) => {
        if (response.ok) {
          response.text();
        } else {
          // error occured or no diagram found
          throw Error('Publish of diagram failed');
        }
      });
      return { type: StopActionType.STOP_ACTION };
    }),
  );
};

export const loadDiagramEpic: Epic<
  Action,
  UpdateDiagramAction | ChangeDiagramTypeAction | DisplayErrorAction,
  ApplicationState
> = (action$) => {
  return action$.pipe(
    ofType(RemoteStorageActionTypes.LOAD),
    map((action) => action as LoadAction),
    mergeMap(async (action: LoadAction) => {
      const { token } = action.payload;
      try {
        const diagram = await RemoteStorageRepository.fetchDiagram(token);
        if (diagram.model?.type) {
          return of(
            DiagramRepository.updateDiagram({ ...diagram }),
            EditorOptionsRepository.changeDiagramType(diagram.model?.type),
          );
        }
        return of(DiagramRepository.updateDiagram({ ...diagram }));
      } catch (error: any) {
        return of(
          ErrorRepository.createError(
            ErrorActionType.DISPLAY_ERROR,
            'Error loading diagram',
            error.message,
          ) as DisplayErrorAction,
        );
      }
    }),
    concatAll(),
  );
};

export const getUserDiagramsEpic: Epic<Action, GetUserDiagramsDoneAction | DisplayErrorAction, ApplicationState> = (
  action$: Observable<Action<RemoteStorageActionTypes>>,
) => {
  return action$.pipe(
    ofType(RemoteStorageActionTypes.GET_USER_DIAGRAMS),
    map((action) => action as GetUserDiagramsAction),
    map(async (_action: GetUserDiagramsAction) => {
      // Server will get the user from the current session
      try {
        const diagrams = await RemoteStorageRepository.fetchUserDiagramsFromServer();
        return RemoteStorageRepository.getUserDiagramsDone(diagrams);
      } catch (error: any) {
        return ErrorRepository.createError(
          ErrorActionType.DISPLAY_ERROR,
          'Error loading diagrams',
          error.message,
        ) as DisplayErrorAction;
      }
    }),
    concatAll(),
  );
};

// TODO: Maybe avoid using any type in decorator?
export const remoteStorageEpics = combineEpics<Action, any, ApplicationState>(
  getUserDiagramsEpic,
  loadDiagramEpic,
  storeEpic,
) as any;
