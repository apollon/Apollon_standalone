import { Reducer } from 'redux';
import { RemoteStorageActionTypes, RemoteStorageState } from './remote-storage-types';
import { Actions } from '../actions';

export const RemoteStorageReducer: Reducer<RemoteStorageState, Actions> = (
  state: RemoteStorageState = { fetchedUserDiagrams: null },
  action,
) => {
  switch (action.type) {
    case RemoteStorageActionTypes.GET_USER_DIAGRAMS_DONE: {
      const { payload } = action;
      return { fetchedUserDiagrams: payload.diagrams };
    }
  }

  return state;
};
