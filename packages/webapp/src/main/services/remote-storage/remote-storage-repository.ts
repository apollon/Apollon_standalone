import { UserDiagramsResultDTO } from 'shared/src/main/user-diagrams-result-dto';
import {
  GetUserDiagramsAction,
  GetUserDiagramsDoneAction,
  LoadAction,
  RemoteStorageActionTypes,
  RemoteStorageDiagramListItem,
  StoreAction,
} from './remote-storage-types';
import { BASE_URL } from '../../constant';
import { Diagram } from '../diagram/diagram-types';
import { DiagramDTO } from 'shared/src/main/diagram-dto';

export const RemoteStorageRepository = {
  getUserDiagrams: (): GetUserDiagramsAction => ({
    type: RemoteStorageActionTypes.GET_USER_DIAGRAMS,
    payload: {},
  }),

  getUserDiagramsDone: (diagrams: UserDiagramsResultDTO[]): GetUserDiagramsDoneAction => ({
    type: RemoteStorageActionTypes.GET_USER_DIAGRAMS_DONE,
    payload: { diagrams },
  }),

  load: (token: string): LoadAction => ({
    type: RemoteStorageActionTypes.LOAD,
    payload: {
      token,
    },
  }),

  store: (diagram: Diagram): StoreAction => ({
    type: RemoteStorageActionTypes.STORE,
    payload: {
      diagram,
    },
  }),

  async fetchUserDiagramsFromServer(): Promise<UserDiagramsResultDTO[]> {
    const resourceUrl = `${BASE_URL}/diagrams/byuser`;
    const response = await fetch(resourceUrl, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    });
    if (response.ok) {
      const result = (await response.json()) as UserDiagramsResultDTO[];
      return result;
    } else {
      throw Error(`Server responded with ${response.status} (${response.statusText})`);
    }
  },

  async fetchDiagram(token: string): Promise<DiagramDTO> {
    const resourceUrl = `${BASE_URL}/diagrams/${token}`;
    const response = await fetch(resourceUrl, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    });
    if (response.ok) {
      const result = (await response.json()) as DiagramDTO;
      return result;
    } else {
      throw Error(`Server responded with ${response.status} (${response.statusText})`);
    }
  },
};
