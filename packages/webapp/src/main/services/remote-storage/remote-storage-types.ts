import { Action } from 'redux';
import { UserDiagramsResultDTO } from 'shared/src/main/user-diagrams-result-dto';
import { UMLDiagramType } from '@ls1intum/apollon';
import { Moment } from 'moment';
import { Diagram } from '../diagram/diagram-types';

export type RemoteStorageActions = GetUserDiagramsAction | GetUserDiagramsDoneAction | LoadAction | StoreAction;

export type RemoteStorageState = {
  // null if not fetched, empty list if fetch returned no diagrams
  fetchedUserDiagrams: UserDiagramsResultDTO[] | null;
};

export const enum RemoteStorageActionTypes {
  GET_USER_DIAGRAMS = '@@remote-storage/get-user-diagrams',
  GET_USER_DIAGRAMS_DONE = '@@remote-storage/get-user-diagrams-done',
  LOAD = '@@remote_storage/load',
  LOAD_LATEST = '@@remote_storage/load_latest',
  STORE = '@@remote_storage/store',
  LIST_STORED = '@@remote_storage/list_stored',
}

export type GetUserDiagramsAction = Action<RemoteStorageActionTypes.GET_USER_DIAGRAMS> & {
  // Do not add user to this payload. The server will get the user from the current session.
  payload: {};
};

export type GetUserDiagramsDoneAction = Action<RemoteStorageActionTypes.GET_USER_DIAGRAMS_DONE> & {
  payload: { diagrams: UserDiagramsResultDTO[] };
};

/// TODO: Used when changing how to load diagrams using remote storage (shows list of diagrams)
export type RemoteStorageDiagramListItem = {
  id: string;
  title: string;
  type: UMLDiagramType;
  lastUpdate: Moment;
};

export type LoadAction = Action<RemoteStorageActionTypes.LOAD> & {
  payload: {
    // id: string;
    token: string;
  };
};

type StorePayload = {
  payload: {
    diagram: Diagram;
  };
};

export type StoreAction = Action<RemoteStorageActionTypes.STORE> & StorePayload;
